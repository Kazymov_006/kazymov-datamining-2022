from django.urls import path

from common.views import main_view

urlpatterns = [
    path("", main_view, name="main"),
]
