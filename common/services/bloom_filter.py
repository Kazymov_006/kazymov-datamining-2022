import random
import re
from math import log, ceil, e
from time import time, sleep

from faker import Faker


def false_positive_rate(m, n, k):
    """
    :param m: amount of S
    :param n: length of BF vector
    :param k: number of hash functions
    :return: false positive rate of bloom filter predictions in percents
    """
    return (1 - e ** (-k * m / n)) ** k * 100


def generate_random_seeds(amount):
    seeds = random.sample(range(100), amount)
    return seeds


def mmix(h):
    h ^= h >> 16
    h = (h * 0x85ebca6b) & 0xFFFFFFFF
    h ^= h >> 13
    h = (h * 0xc2b2ae35) & 0xFFFFFFFF
    h ^= h >> 16
    return h


def murmur3hash(k, seed):
    key = bytearray(k.encode("utf-8"))
    length = len(key)
    nblocks = int(length / 4)

    h1 = seed

    c1 = 0xcc9e2d51
    c2 = 0x1b873593

    # body
    for block_start in range(0, nblocks * 4, 4):
        # ??? big endian?
        k1 = (
                key[block_start + 3] << 24 |
                key[block_start + 2] << 16 |
                key[block_start + 1] << 8 |
                key[block_start + 0]
        )

        k1 = (c1 * k1) & 0xFFFFFFFF
        k1 = (k1 << 15 | k1 >> 17) & 0xFFFFFFFF  # inlined ROTL32
        k1 = (c2 * k1) & 0xFFFFFFFF

        h1 ^= k1
        h1 = (h1 << 13 | h1 >> 19) & 0xFFFFFFFF  # inlined ROTL32
        h1 = (h1 * 5 + 0xe6546b64) & 0xFFFFFFFF

    # tail
    tail_index = nblocks * 4
    k1 = 0
    tail_size = length & 3

    if tail_size >= 3:
        k1 ^= key[tail_index + 2] << 16
    if tail_size >= 2:
        k1 ^= key[tail_index + 1] << 8
    if tail_size >= 1:
        k1 ^= key[tail_index + 0]

    if tail_size > 0:
        k1 = (k1 * c1) & 0xFFFFFFFF
        k1 = (k1 << 15 | k1 >> 17) & 0xFFFFFFFF  # inlined ROTL32
        k1 = (k1 * c2) & 0xFFFFFFFF
        h1 ^= k1

    # finalization
    unsigned_val = mmix(h1 ^ length)
    if unsigned_val & 0x80000000 == 0:
        return unsigned_val
    else:
        return -((unsigned_val ^ 0xFFFFFFFF) + 1)


extra_symbols = re.compile(r"(?:(?:-*[^a-zA-Z'\-]+-*)|(?:--+))")

with open("macbeth.txt", "r", encoding="utf-8") as book:
    words_list = extra_symbols.split(book.read())

n = len(words_list)
dictionary = {}
for word in words_list:
    word = word.lower()
    try:
        dictionary[word] += 1
    except KeyError:
        dictionary[word] = 1

assert sum(dictionary.values()) == n, "Calculational error"
print(n)

m = len(dictionary)
n = m * 5  # for fp rate < 0.1
k = ceil(log(2) * n / m)  # for our case it gives 4

seeds = generate_random_seeds(k)
bloom_f = [0 for i in range(n)]


class Bloom:
    def __init__(self, m, n, k):
        self.m = m
        self.n = n
        self.k = k
        self.seeds = generate_random_seeds(k)
        self.bloom_f = [0 for _ in range(n)]

    def add_word_in_bloom(self, word, amount=1):
        word = word.lower()
        for seed in self.seeds:
            idx = murmur3hash(word, seed) % n
            self.bloom_f[idx] += amount

    def remove_word_from_bloom(self, word):
        word = word.lower()
        indices = []
        for seed in self.seeds:
            idx = murmur3hash(word, seed) % n
            indices.append(idx)
            if self.bloom_f[idx] == 0:
                return False
        for idx in indices:
            self.bloom_f[idx] -= 1
        return True

    def word_in_dictionary(self, word):
        word = word.lower()
        for seed in self.seeds:
            idx = murmur3hash(word, seed) % n
            if self.bloom_f[idx] == 0:
                return False
        return True

    def test_false_positive_rate(self, n_tests):
        fake = Faker()
        false_positives = 0
        true_negatives = 0
        for _ in range(n_tests):
            name = fake.name()
            pred = self.word_in_dictionary(name)
            real = name.lower() in dictionary.keys()
            if not real:
                if pred:
                    false_positives += 1
                else:
                    true_negatives += 1

        return false_positives * 100 / (false_positives + true_negatives)



if __name__ == '__main__':
    print(*[f"{key} - {val}" for key, val in sorted(dictionary.items(),
                                                    key=lambda x: x[1])],
          sep="\n")
    print(m, n, k)
    bloom = Bloom(m, n, k)
    # filling bloom
    for word, amount in dictionary.items():
        bloom.add_word_in_bloom(word, amount)
    print(false_positive_rate(m, n, k))
    print("-" * 120)
    print(bloom.word_in_dictionary("death"))
    print(bloom.test_false_positive_rate(1000))

    beg = time()

    bloom.word_in_dictionary("imaginary_word")

    end = time()
    diff1 = end - beg
    words_list = [word.lower() for word in words_list]
    beg = time()

    "imaginary_word" in words_list

    end = time()
    diff2 = end - beg
    test_cases_num = 1000000
    response = (
        f"search with bloom filter = {diff1}\n"
        f"search with python operator 'in' = {diff2}\n"
        f"bloom filter is faster {diff2 / diff1} times.\n"
        f"Predicted fpr = {round(false_positive_rate(m, n, k), 2)}%\n"
        f"Real fpr for {test_cases_num} tests = "
        f"{round(bloom.test_false_positive_rate(test_cases_num), 2)}%"
    )
    with open("output.txt", "w", encoding="utf-8") as file:
        file.write(response)
