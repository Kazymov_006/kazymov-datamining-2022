from django.urls import re_path

from pagerank.views import main_view

urlpatterns = [
    re_path(r"/?", main_view, name="main"),
]
