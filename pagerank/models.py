from django.db import models


# Create your models here.

class Site(models.Model):
    link = models.URLField()
    rank = models.FloatField(default=1.0)
    group = models.ForeignKey("Group", on_delete=models.CASCADE,
                              related_name="sites")


class Group(models.Model):
    root = models.ForeignKey(
        Site,
        related_name="sites",
        on_delete=models.CASCADE, null=True, blank=True
    )
    created_at = models.DateTimeField(auto_created=True, auto_now=True)

