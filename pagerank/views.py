from django.shortcuts import render

from pagerank.forms import SiteForm
from pagerank.tasks import calculate_page_rank


def main_view(request):
    context = {"form": SiteForm(), "alert": False}
    if request.method == "POST":
        form = SiteForm(request.POST)
        context["form"] = form
        if form.is_valid():
            link = form.cleaned_data["link"]
            print("everything is cool")
            calculate_page_rank.delay(link)
            print("everything is very cool")
        context["alert"] = "Wait for it. You can return to site in a few hours."
    return render(request, "pagerank/main.html", context)
