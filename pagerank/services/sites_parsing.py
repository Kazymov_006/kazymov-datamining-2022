import json
import re

import requests

from bs4 import BeautifulSoup


def is_valid(link):
    try:
        response = requests.get(link, allow_redirects=True)
        link = response.url
    except requests.exceptions.ConnectionError as e:
        link = e.request.url
    except requests.exceptions.InvalidURL as e:
        return False, None

    return True, link


def get_links_from(url):
    def process_link(a):
        try:
            href = a.get("href", False)
            if not href:
                link = None
            else:
                resp = requests.get(href, allow_redirects=True, timeout=10)
                link = resp.url
        except requests.exceptions.Timeout:
            link = href
        except Exception as e:
            link = None
        if link is not None:
            link = link.split("?")[0]
        return link

    try:
        response = requests.get(url, allow_redirects=True)
    except requests.exceptions.ConnectionError as e:
        links = set()
    else:
        soup = BeautifulSoup(response.text, "lxml")
        links = set(filter(lambda x: x is not None,
                           map(process_link, soup.find_all("a"))))
    print(links)
    return links


if __name__ == '__main__':
    with open("sites.txt", "r", encoding="utf-8") as file:
        # sites = re.sub(r"(?!(?:{(?:'[^}{]*')?})|(?:set\(\)))",
        #                "set()", file.read())
        sites = re.findall(r"(?:{(?:'[^}{]*')?})|(?:set\(\))", file.read())
        print(sites)
        print(len(sites))

    s = 0
    all_sites = set()
    for i in range(len(sites)):
        links = sites[i]
        if links == 'set()':
            links = set()
        else:
            links = set(
                links
                .replace("'", "")
                .replace("{", "")
                .replace("}", "")
                .replace(" ", "")
                .replace("\n", "")
                .split(",")
            )
        sites[i] = links
        s += len(links)
        all_sites.update(links)

    print(s)
    print(len(all_sites))

    from pagerank import SitesGraph, SiteVertex

    graph = SitesGraph()
    """
    def register_children_directly(self, vertex=None, depth=0):
        if depth == 3:
            return
        if vertex is None:
            vertex = self.root
        children = get_links_from(vertex.link)
        for child_link in children:
            child = self.get_or_create_site_vertex(child_link)
            child.parents.add(vertex)
            vertex.children.add(child)
            self.register_children_directly(child, depth + 1)
        """
    root_site = graph.get_or_create_site_vertex("https://medium.com/")
    temp_sites = sites[:]
    children0 = set()
    ss = 0
    print(sites)
    for link_ in sites.pop(0):
        vert = graph.get_or_create_site_vertex(link_)
        vert.parents.add(root_site)
        root_site.children.add(vert)
        children0.add(vert)
        print(link_, vert)
    print(children0)

    for parent in children0:
        children1 = set()
        try:
            aeaea = sites.pop(0)
        except IndexError:
            aeaea = set()
        for link__ in aeaea:
            vert = graph.get_or_create_site_vertex(link__)
            vert.parents.add(parent)
            parent.children.add(vert)
            children1.add(vert)
        ss += len(children1)
        for parent2 in children1:
            try:
                aeaea = sites.pop(0)
            except IndexError:
                aeaea = set()
            for link___ in aeaea:
                vert = graph.get_or_create_site_vertex(link___)
                vert.parents.add(parent2)
                parent2.children.add(vert)
    print("ss", ss)

    graph.update_ranks(n_iterations=100, beta=0.85)

    print(graph)
    print(len(graph))
    with open("results1.json", "w") as res:
        json.dump(graph.get_data_for_db(), res)
    with open("results1.txt", "w") as res:
        for site in graph.get_data_for_db():
            res.write(f"{site['link']} - {site['rank']}\n")

    graph.norm()
    with open("results1_normed.json", "w") as res:
        json.dump(graph.get_data_for_db(), res)
    with open("results1_normed.txt", "w") as res:
        for site in graph.get_data_for_db():
            res.write(f"{site['link']} - {site['rank']}\n")
