import json
from typing import List, Optional

from sites_parsing import is_valid, get_links_from


class SiteVertex:
    def __init__(self, link):
        self.link = link
        self.rank = 1.0
        self.children = set()
        self.parents = set()

    def get_updated_update_rank(self, beta, vertexes_num):
        assert 0 < beta <= 1, "Dumping factor must be between 0 and 1"
        sum_of_prs = sum((page.rank / len(page.children))
                         for page in self.parents)
        new_rank = beta * sum_of_prs + (1 - beta) / vertexes_num
        return new_rank


class SitesGraph:
    def __init__(self):
        self.vertexes: List["SiteVertex"] = []
        self.root: Optional["SiteVertex"] = None

    def register_children_directly(self, vertex=None, depth=0):
        if depth == 3:
            return
        if vertex is None:
            vertex = self.root
        children = get_links_from(vertex.link)
        for child_link in children:
            child = self.get_or_create_site_vertex(child_link)
            child.parents.add(vertex)
            vertex.children.add(child)
            self.register_children_directly(child, depth + 1)

    def compose_directly(self, link):
        url_is_valid, link = is_valid(link)
        if url_is_valid:
            self.root = self.get_or_create_site_vertex(link)
            self.register_children_directly()
            for vert in self.vertexes:
                vert.rank /= len(self)
            return True
        return False

    def compose_from_db(self, link):
        pass

    def get_or_create_site_vertex(self, link):
        for vertex in self.vertexes:
            if vertex.link == link:
                return vertex

        site = SiteVertex(link)
        self.vertexes.append(site)
        if self.root is None:
            self.root = site
        return site

    def update_ranks_one_time(self, beta):
        new_ranks = []
        for node in self.vertexes:
            new_ranks.append(node.get_updated_update_rank(
                beta, vertexes_num=len(self)))

        for node, new_rank in zip(self.vertexes, new_ranks):
            node.rank = new_rank

    def update_ranks(self, beta=0.85, n_iterations=50):
        for _ in range(n_iterations):
            self.update_ranks_one_time(beta)

    def get_page_ranks(self):
        res = {}
        for node in self.vertexes:
            res[node.link] = node.rank
        return res

    def get_data_for_db(self):
        lines = []
        for node in self.vertexes:
            vert = {
                "link": node.link,
                "rank": node.rank,
            }
            lines.append(vert)
        lines.sort(key=lambda elem: elem["rank"], reverse=True)
        return lines

    def norm(self):
        s = sum(vert.rank for vert in self.vertexes)
        for vert in self.vertexes:
            vert.rank /= s

    def __str__(self):
        return "\n".join(f"{node.link} - {node.rank}" for node in self.vertexes)

    def __len__(self):
        return len(self.vertexes)


if __name__ == '__main__':
    # get_links_from("https://medium.com/")
    graph = SitesGraph()
    graph.compose_directly("https://medium.com/")
    # a = graph.get_or_create_site_vertex("a")
    # b = graph.get_or_create_site_vertex("b")
    # c = graph.get_or_create_site_vertex("c")
    # d = graph.get_or_create_site_vertex("d")
    # print(graph)
    # b.parents.add(a)
    # c.parents.add(a)
    # c.parents.add(b)
    # d.parents.add(a)
    # a.children.update({b, c, d})
    # b.children.add(c)
    # a.parents.add(c)
    # c.children.add(a)
    graph.update_ranks(beta=0.85, n_iterations=50)
    with open("results.json", "w") as res:
        json.dump(graph.get_data_for_db(), res)
    graph.norm()
    with open("results_normed.json", "w") as res:
        json.dump(graph.get_data_for_db(), res)




