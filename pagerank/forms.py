from django.forms import forms, fields

from pagerank.services.sites_parsing import is_valid


class BootstrapFormMixin:
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in self.fields.values():
            field.widget.attrs["class"] = field.widget.attrs.get("class", "")
            field.widget.attrs["class"] += " form-control"


class SiteForm(BootstrapFormMixin, forms.Form):
    link = fields.URLField()

    def clean(self):
        cleaned_data = super(SiteForm, self).clean()
        if not is_valid(cleaned_data["link"]):
            self.add_error(None, "Link is not valid")
        return cleaned_data
