import django

if __name__ == '__main__':
    django.setup()

from pagerank.models import Group, Site
from celery import shared_task
from pagerank.services.pagerank import SitesGraph


@shared_task
def calculate_page_rank(link):
    group = Group(link)
    group.save()
    graph = SitesGraph()
    graph.compose_directly(link)
    graph.update_ranks(beta=0.85, n_iterations=50)
    Site.objects.bulk_create(
        [
            Site(
                link=node.link,
                rank=node.rank,
                group=group
            ) for node in graph.vertexes
        ]
    )


if __name__ == '__main__':
    calculate_page_rank.delay("https://www.kaggle.com/competitions")
