from django.urls import path

from webcrawlerbloggersad.views import statistics_view

urlpatterns = [
    path("", statistics_view, name="main"),
]
