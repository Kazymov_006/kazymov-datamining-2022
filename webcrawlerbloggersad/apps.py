from django.apps import AppConfig


class WebcrawlerbloggersadConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'webcrawlerbloggersad'
