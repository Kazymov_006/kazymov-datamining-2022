from django.contrib import admin

# Register your models here.
from webcrawlerbloggersad.models import Video, Link


class LinkInline(admin.TabularInline):
    model = Link
    fields = ("id", "link")
    readonly_fields = ("id",)


@admin.register(Video)
class VideoAdmin(admin.ModelAdmin):
    readonly_fields = (
        "id",
        "title",
        "description",
        "date",
        "tags",
        "like_count",
        "view_count",
        "comment_count",
    )
    list_display = (
        "id",
        "title",
        "date",
        "like_count",
        "view_count",
        "comment_count",
    )
    list_display_links = (
        "id",
        "title",
    )
    inlines = (LinkInline,)


@admin.register(Link)
class LinkAdmin(admin.ModelAdmin):
    readonly_fields = ("id", )
    list_display = ("id", "link", "video_id")
    list_display_links = ("id", )
