import httplib2
import apiclient.discovery
from oauth2client.service_account import ServiceAccountCredentials

from dataminingtasks import settings

if __name__ == '__main__':
    import django
    django.setup()

from webcrawlerbloggersad.models import Link

CREDENTIALS_FILE = 'mypython-318607-87486d8d9e48.json'

credentials = ServiceAccountCredentials.from_json_keyfile_name(
    CREDENTIALS_FILE,
    [
        'https://www.googleapis.com/auth/spreadsheets',
        'https://www.googleapis.com/auth/drive'
    ]
)

httpAuth = credentials.authorize(httplib2.Http())
service = apiclient.discovery.build('sheets', 'v4', http=httpAuth)

# spreadsheet = {
#     'properties': {
#         'title': "Advertisement on ITPedia channel"
#     }
# }
# spreadsheet = service.spreadsheets().create(body=spreadsheet,
#                                             fields='spreadsheetId').execute()
# print('Spreadsheet ID: {0}'.format(spreadsheet.get('spreadsheetId')))


spreadsheetId = "1nMUdO2f_5znh_tvrxxsWrSDj1SOSmQxsthW7MIMtkwM"


def fill_spreadsheet(spreadsheetId):
    driveService = apiclient.discovery.build('drive', 'v3', http=httpAuth)
    access = driveService.permissions().create(
        fileId=spreadsheetId,
        body={
            'type': 'user',
            'role': 'writer',
            'emailAddress': 'rayskarken@gmail.com'
        },  # Открываем доступ на редактирование
        fields='id'
    ).execute()

    values_for_spreadsheet = [
        [
            'title',
            'description',
            'date',
            'tag',
            'like_count',
            'view_count',
            'comment_count',
            'adv_link'
        ]
    ]
    for link in Link.objects.all():
        values_for_spreadsheet.extend(
            [
                [
                    link.video.title,
                    link.video.description,
                    str(link.video.date),
                    tag,
                    link.video.like_count,
                    link.video.view_count,
                    link.video.comment_count,
                    link.link
                ] for tag in link.video.tags
            ]
        )
    results = service.spreadsheets().values().batchUpdate(
        spreadsheetId=spreadsheetId, body={
            "valueInputOption": "USER_ENTERED",
            # Данные воспринимаются, как вводимые пользователем
            # (считается значение формул)
            "data": [
                {"range": "Sheet1!A1:H20001",
                 "majorDimension": "ROWS",     # Сначала заполнять строки, затем столбцы
                 "values": values_for_spreadsheet}
            ]
        },

    ).execute()
    print(results)


def main():
    fill_spreadsheet(spreadsheetId)


if __name__ == '__main__':
    main()
    # полученная таблица - https://docs.google.com/spreadsheets/d/1nMUdO2f_5znh_tvrxxsWrSDj1SOSmQxsthW7MIMtkwM/edit#gid=0
