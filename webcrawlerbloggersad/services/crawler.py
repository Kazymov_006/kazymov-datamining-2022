import re
from multiprocessing import Pool

import requests
from googleapiclient.discovery import build
import django

if __name__ == '__main__':
    django.setup()

from dataminingtasks.settings import GOOGLE_API_KEY

from webcrawlerbloggersad.models import Video, Link

DEVELOPER_KEY = GOOGLE_API_KEY
YOUTUBE_API_SERVICE_NAME = 'youtube'
YOUTUBE_API_VERSION = 'v3'


def get_all_videos_from_channel_with(channel_id="UC6bTF68IAV1okfRfwXIP1Cg"):
    youtube = build(YOUTUBE_API_SERVICE_NAME, YOUTUBE_API_VERSION,
                    developerKey=DEVELOPER_KEY)

    # Call the search.list method to retrieve results matching the specified
    # query term.
    search_response = youtube.search().list(
        channelId=channel_id,
        part='id',
        maxResults=50,
        type='video'
    ).execute()
    nextPageToken = search_response.get("nextPageToken", False)
    video_indices = [item["id"]["videoId"] for item in search_response["items"]]
    videos_snippets = youtube.videos().list(
        id=video_indices,
        part=['snippet', 'statistics']
    ).execute()
    videos_info = [
        {
            "title": item["snippet"]["title"],
            "description": item["snippet"].get("description", "NO DESCRIPTION"),
            "date": item["snippet"]["publishedAt"],
            "tags": item["snippet"].get("tags", []),
            "like_count": item["statistics"].get("likeCount", 0),
            "view_count": item["statistics"].get("viewCount", 0),
            "comment_count": item["statistics"].get("commentCount", 0)
        }
        for item in videos_snippets["items"]
    ]

    while nextPageToken:
        search_response = youtube.search().list(
            channelId=channel_id,
            part='id',
            maxResults=50,
            type='video',
            pageToken=nextPageToken
        ).execute()
        nextPageToken = search_response.get("nextPageToken", False)
        video_indices = [item["id"]["videoId"] for item in
                         search_response["items"]]
        videos_snippets = youtube.videos().list(
            id=video_indices,
            part=['snippet', 'statistics']
        ).execute()
        videos_info.extend([
            {
                "title": item["snippet"]["title"],
                "description": item["snippet"].get("description",
                                                   "NO DESCRIPTION"),
                "date": item["snippet"]["publishedAt"],
                "tags": item["snippet"].get("tags", []),
                "like_count": item["statistics"].get("likeCount", 0),
                "view_count": item["statistics"].get("viewCount", 0),
                "comment_count": item["statistics"].get("commentCount", 0)
            }
            for item in videos_snippets["items"]
        ])
    return videos_info


def fill_an_empty_db(videos, links):
    try:
        videos[0]["title"], videos[0]["description"], videos[0]["date"], \
        videos[0]["tags"], videos[0]["like_count"], videos[0]["comment_count"], \
        videos[0]["view_count"]
    except KeyError:
        return False
    Video.objects.bulk_create([Video(**video) for video in videos])
    for video_id, links_ in links:
        video = Video.objects.get(id=video_id)
        for link_ in links_:
            link = Link(link=link_, video=video)
            link.save()


def extract_row_links_from_text(text):
    result = re.findall(r"https?:\/\/(?:www\.)?[-a-zA-Z0-9@:%._\+~#=]+\.["
                        r"a-zA-Z0-9()]+\b(?:[-a-zA-Z0-9()@:%_\+.~#?&//=]*)",
                        text)
    return result


def extract_adv_links(text):
    social_networks = [
        "vk",
        "youtube",
        "facebook",
        "twitch",
        "twitter",
        "t.me",
        "telegram"
    ]
    app_markets = [
        "steampowered",
        "apps.apple",
        "play.google"
    ]
    row_links = extract_row_links_from_text(text)
    results = []
    for link in row_links:
        try:
            response = requests.get(link, allow_redirects=True)
            link = response.url
        except requests.exceptions.ConnectionError as e:
            link = e.request.url
        if any(map(lambda x: x in link, app_markets)):
            results.append(link)
            continue
        result = re.findall(r"https?:\/\/(?:www\.)?[-a-zA-Z0-9@:%._\+~#=]+\.["
                            r"a-zA-Z0-9()]+\b",
                            link)[0]
        if not any(map(lambda x: x in result, social_networks)):
            results.append(result)
    return results


def get_links_from_video(video_idx):
    return video_idx[1], extract_adv_links(video_idx[0]["description"])


def get_links_from_videos(videos):
    indices = [i for i in range(1, len(videos) + 1)]
    with Pool() as cpu_kernels_pool:
        links = cpu_kernels_pool.map(get_links_from_video,
                                     zip(videos, indices))
    return links


def empty_db():
    Link.objects.all().delete()
    Video.objects.all().delete()
    cursor = django.db.connection.cursor()
    cursor.execute("""
    ALTER SEQUENCE videos_id_seq RESTART;
    ALTER SEQUENCE links_id_seq RESTART;
    """)


def get_data_and_fill_db():
    videos = get_all_videos_from_channel_with()
    links = get_links_from_videos(videos)
    empty_db()
    fill_an_empty_db(videos, links)


if __name__ == '__main__':
    get_data_and_fill_db()
