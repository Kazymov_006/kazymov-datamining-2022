from django.db import models
from django.contrib.postgres.fields import ArrayField


# Create your models here.

class Video(models.Model):
    title = models.CharField(max_length=256)
    description = models.TextField()
    date = models.DateTimeField()
    tags = ArrayField(models.CharField(max_length=256))
    like_count = models.BigIntegerField()
    view_count = models.BigIntegerField()
    comment_count = models.BigIntegerField()

    class Meta:
        db_table = "videos"


class Link(models.Model):
    link = models.URLField()
    video = models.ForeignKey(Video, on_delete=models.PROTECT)

    class Meta:
        db_table = "links"
