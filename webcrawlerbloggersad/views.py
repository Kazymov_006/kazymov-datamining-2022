from django.shortcuts import render

# Create your views here.


def statistics_view(request):
    context = {"iframes_urls": [
        "https://datalens.yandex/v1xkgtndg7x2n",
        "https://datalens.yandex/flh204r0rs927"
    ]}
    return render(request, "webcrawlerbloggersad/main.html", context)
